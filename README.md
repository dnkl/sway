# Sway fork using fontconfig+[fcft](https://codeberg.org/dnkl/fcft) instead of pango for font rendering

## Intro

This is a fork of https://github.com/swaywm/sway that replaces pango
with fontconfig+[fcft](https://codeberg.org/dnkl/fcft) for font
rendering.

I did this because starting with release 1.44, pango no longer
supports bitmap fonts.

This is currently a very quick and dirty hack; it rips out pango, not
with surgical precision, but with a sledge hammer (imaging trying to
rip out _anything_ with a sledge hammer...). The idea is to keep
changes to regular sway sources down to a minimum, to make it easier
to keep up-to-date with sway.

Currently, **only** the sway main binary has been ported. That is, no
swaybar and no swaynag support.

The config syntax has changed, and now uses fontconfig syntax rather
than pango syntax. That is, what used to be written as:

    font MyFont 12px

is now written:

    font MyFont:pixelsize=12

## Instructions

I suggest you install sway like you usually do (e.g. use your distro's
package), and then replace the installed sway binary with the one
built here. Make sure you match the version!

First, checkout one of the _fontconfig_ branches:

* `fontconfig` -> `master`
* `fontconfig-v1.2` -> `v1.2`
* and so on

Then, configure (disabling swaybar and swaynag since they are not
supported), build, and install:

    meson -Dswaybar=false -Dswaynag=false build/
    ninja -C build/
    sudo ninja -C build/ install

Finally, don't forget to update the sway config to use the new font syntax!

## Limitations:

* Swaybar not ported
* Swaynag not ported
* No markup
