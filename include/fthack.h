#pragma once

/* The various components of sway often forget to pull in headers that they get for free from e.g. pango */
#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <limits.h>
#include <signal.h>
#include <sys/types.h>

typedef void PangoLayout;
typedef void PangoContext;

#define pango_cairo_font_map_set_default(ptr)
#define pango_cairo_create_context(cr) (void *)NULL;
#define pango_layout_set_width(layout, width)
#define pango_layout_set_wrap(layout, wrap)
#define pango_layout_set_single_paragraph_mode(layout, mode)
#define pango_cairo_update_layout(cr, layout)
#define g_object_unref(ptr) (void)(ptr)
